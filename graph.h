#ifndef GRAPH_H
#define GRAPH_H

#include "node.h"
#include "mathhelper.h"
#include <stack>
#include <iostream>

#define NULL 0

using namespace std;

struct Circuit{
    stack<Path*> paths;
    int weight;
};

class Graph
{
    public:
        Graph();

        Graph(int size);

        bool addNode();

        bool addPath(int a, int b, int weight);

        Node* getRoot();

        int size();

        vector<Path*> shortestPath();

    private:

        void shortestPath(Node* start, Node* it, stack<Path*> visited);

        //Auxiliar vector with circuit
        vector<Circuit*> circuits;

        //Number of Nodes
        int N;

        //Root of the node list
        Node* root;

        //Last node of the list
        Node* last;
};

#endif // GRAPH_H
