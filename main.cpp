#include <QCoreApplication>
#include "graph.h"
#include "filehelper.h"
#include <stack>



using namespace std;

int print(stack<int> v){
    v.pop();
}

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    Graph* g = FileHelper::loadGraph("data/graph.data");

    g->shortestPath();

    FileHelper::saveGraph("data/graph.data",g);
    return a.exec();
}
