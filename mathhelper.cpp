#include "mathhelper.h"

bool MathHelper::greaterThan(vector<Path *> a, vector<Path *> b){
    int a_value = 0;
    int b_value = 0;

    for(int i = 0; i < a.size(); i++){
        a_value += a[i]->getWeight();
    }

    for(int i = 0; i < b.size(); i++){
        b_value += b[i]->getWeight();
    }

    return a_value > b_value;
}

int MathHelper::weight(vector<Path*> v){
    int w = 0;

    for(int i = 0; i < v.size(); i++){
        w += v[i]->getWeight();
    }

    return w;
}

int MathHelper::weight(stack<Path*> v){
    int w = 0;

    while(v.size() > 0){
        w += v.top()->getWeight();
        v.pop();
    }

    return w;
}
