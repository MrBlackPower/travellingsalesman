#ifndef MATHHELPER_H
#define MATHHELPER_H

#include <vector>
#include <stack>
#include "path.h"

using namespace std;

class MathHelper
{
public:
    static bool greaterThan(vector<Path*> a, vector<Path*> b);
    static int weight(vector<Path*> v);
    static int weight(stack<Path*> v);
};

#endif // MATHHELPER_H
