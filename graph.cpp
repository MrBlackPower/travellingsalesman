#include "graph.h"

Graph::Graph()
{
    N = 0;
    root = NULL;
    last = NULL;
    circuits.clear();
}

Graph::Graph(int size){
    N = 0;
    root = NULL;
    last = NULL;
    circuits.clear();

    for(int i = 0; i < size; i ++){
        addNode();
    }
}

bool Graph::addNode(){
    Node* n = new Node(N);

    if(N == 0){
        root = n;
        last = n;
    } else {
        last->setNext(n);
        last = n;
    }

    N++;
    return true;
}

bool Graph::addPath(int a, int b, int weight){
    if(a >= N || b >= N)
        return false;

    if(a < 0 || b < 0)
        return false;

    Node* it = root;
    Node* A = NULL;
    Node* B = NULL;

    while(it != NULL){
        if(Node::equals(it,a))
            A = it;

        if(Node::equals(it,b))
            B = it;

        it = it->getNext();
    }

    if(Node::equals(A,B))
        return false;

    if(A == NULL || B == NULL)
        return false;

    if(Node::endingIn(B,A->getAdjacencyList()) || Node::endingIn(A,B->getAdjacencyList()))
        return false;

    A->addAdjacent(B,weight);
    B->addAdjacent(A,weight);

    return true;
}

Node* Graph::getRoot(){
    return root;
}

int Graph::size(){
    return N;
}

vector<Path*> Graph::shortestPath(){
    Node* it = root;
    Circuit* min;
    stack<Path*> v;

    circuits.clear();

    shortestPath(root,root,v);


    //CHOOSES THE SMALLEST CIRCUIT
    cout << " THE TRAVELING SALES POSTMAN " << endl;
    cout << " BRUTE FORCE, #CASES=" << circuits.size() << endl;
    for(int i = 0; i < circuits.size(); i ++){
        Node::printPaths(circuits[i]->paths);
        if(i != 0){
            if(min->weight > circuits[i]->weight)
                min = circuits[i];
        } else {
            min = circuits[i];
        }
    }
    cout << endl << endl << "RESULT: (W= " << min->weight << ")" << endl;
    Node::printPaths(min->paths);

    circuits.clear();
}

void Graph::shortestPath(Node* start, Node* it, stack<Path*> visited){
    if(start == NULL || it == NULL)
        return;

    //VISITED EVERY NODE
    if(visited.size() >= N){
        if(Node::equals(start,it) && visited.size() == N){
            Circuit* c = new Circuit();
            c->paths = visited;
            c->weight = MathHelper::weight(visited);
            circuits.push_back(c);
        } else {
            cout << " -- ERROR ON TSP --" << endl;
        }
    } else if(visited.size() == N - 1) {
        //TRIES TO COMPLETE THE CIRCUIT
        vector<Path*> adj = it->getAdjacencyList();
        for(int i = 0; i < adj.size(); i++){
            Path* p = adj[i];
            if(Node::equals(start,p->getEnd())){
                visited.push(p);
                shortestPath(start,p->getEnd(),visited);
                visited.pop();
            }
        }
    }else {
        vector<Path*> adj = it->getAdjacencyList();
        for(int i = 0; i < adj.size(); i++){
            Path* p = adj[i];
            //ADDS NOT IF NOT VISITED
            if((!Node::endingIn(p->getEnd(),visited)) && (!Node::equals(start,p->getEnd()))){
                visited.push(p);
                shortestPath(start,p->getEnd(),visited);
                visited.pop();
            }
        }
    }
}
