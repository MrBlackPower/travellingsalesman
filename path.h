#ifndef PATH_H
#define PATH_H

#include <iostream>
#include <stack>

using namespace std;

class Node;

class Path
{
public:
    Path(Node* n1, Node* n2, int weight);
    ~Path();

    Node* getStart();

    Node* getEnd();

    int getWeight();
private:
    //Starting node
    Node* n1;

    //Finish node
    Node* n2;

    //Weight of the path
    int weight;
};

#endif // PATH_H
