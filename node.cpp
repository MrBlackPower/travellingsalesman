#include "node.h"

Node::Node(int id)
{
    this->id = id;
    next = NULL;
    adjacency.clear();
}

void Node::addAdjacent(Node *adj, int weight){
    Path* p = new Path(this,adj,weight);
    adjacency.push_back(p);
}

void Node::removeAdjacent(Node *adj){
    removeAdjacent(adj->getId());
}

void Node::removeAdjacent(int id){
    for(int i = 0; i < adjacency.size(); i++){
        Path* p = adjacency[i];
        if(Node::equals(p->getEnd(),id)){
            adjacency.erase(adjacency.begin() + i);
            return;
        }
    }
}

bool Node::equals(Node *n1, int id){
    if(n1->getId() == id)
        return true;

    return false;
}

bool Node::equals(Node *n1, Node *n2){
    if(n1->getId() == n2->getId())
        return true;

    return false;
}

int Node::getId(){
    return id;
}

Node* Node::getNext(){
    return next;
}

void Node::setNext(Node* next){
    this->next = next;
}

vector<Path*> Node::getAdjacencyList(){
    return adjacency;
}

void Node::printPaths(stack<Path *> v){
    cout << endl << endl << "  ------------ PATH END ------------ " << endl ;
    int w = 0;
    while(v.size() > 0){
        Path* p = v.top();

        cout << "START: " << p->getStart()->getId() << "  ->  END: " << p->getEnd()->getId() << "  :  w= " << p->getWeight() << endl;

        w+= p->getWeight();
        v.pop();
    }
    cout << "TOTAL WEIGHT: " << w << endl;
    cout << "  ------------ PATH START ------------ " << endl ;

}

bool Node::endingIn(Node* n, stack<Path*> v){

    while (v.size() > 0){
        Path* p = v.top();

        if(Node::equals(n,p->getEnd())){
            return true;
        }

        v.pop();
    }

    return false;
}

bool Node::endingIn(Node* n, vector<Path*> v){

    for (int i= 0; v.size() > i; i++){
        Path* p = v[i];

        if(Node::equals(n,p->getEnd())){
            return true;
        }
    }

    return false;
}
