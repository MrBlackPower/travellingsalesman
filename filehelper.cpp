#include "filehelper.h"

Graph* FileHelper::loadGraph(QString fileName){
    FILE *file;
    file = fopen(fileName.toStdString().c_str(),"r");
    Graph* g;
    char line[80];

    if(file == NULL){
        cout << "NO FILE OPENED OR NO GRAPH LOADED" << endl;
        return false;
    }

    if(fgets(line, 80, file) != NULL){
        int N = 0;
        QString s(line);
        QTextStream stream(&s);

        //FIRST IT READS THE SIZE
        stream >> N;

        g = new Graph(N);
    } else {
        return NULL;
    }

    while(fgets(line, 80, file) != NULL){
        int a = 0;
        int b = 0;
        int w = 0;
        QString s(line);
        QTextStream stream(&s);

        //THEN THE PATHS
        stream >> a >> b >> w;

        g->addPath(a,b,w);
    }

    return g;
}

bool FileHelper::saveGraph(QString fileName, Graph* g){
    FILE *file;
    file = fopen(fileName.toStdString().c_str(),"w+");

    if(file == NULL || g == NULL){
        cout << "NO FILE OPENED OR NO GRAPH LOADED" << endl;
        return false;
    }

    Node* r = g->getRoot();

    //FIRST LINE IS THE SIZE
    int N = g->size();
    fprintf(file,"%d\n",N);

    vector<Path*> paths;
    paths.clear();


    //THEN PRINTS [START] [END] [WEIGHT]
    while (r != NULL) {
        vector<Path*> adj = r->getAdjacencyList();
        for(int i = 0; i < adj.size(); i++){
            Path* p = adj[i];
            int a = p->getStart()->getId();
            int b = p->getEnd()->getId();
            int w = p->getWeight();
            fprintf(file,"%d %d %d\n",a,b,w);
        }
        r = r->getNext();
    }

    fclose(file);

    cout << " -- SAVED GRAPH --" << endl;

    return true;
}
