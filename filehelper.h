#ifndef FILEHELPER_H
#define FILEHELPER_H

#include "graph.h"
#include <stack>
#include <QString>
#include <QTextStream>
#include <iostream>

using namespace std;

class FileHelper
{
    public:
        static Graph* loadGraph(QString fileName);

        static bool saveGraph(QString fileName, Graph* g);
};

#endif // FILEHELPER_H
