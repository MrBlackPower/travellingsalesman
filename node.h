#ifndef NODE_H
#define NODE_H

#include <iostream>
#include <vector>
#include "path.h"

#define NULL 0

using namespace std;

class Node
{
    public:
        Node(int id);

        void addAdjacent(Node* adj, int weight);

        void removeAdjacent(Node* adj);

        void removeAdjacent(int id);

        static bool equals(Node* n1, Node* n2);

        static bool equals(Node* n1, int id);

        static void printPaths(stack<Path*> v);

        static bool endingIn(Node* n, stack<Path*> v);

        static bool endingIn(Node* n, vector<Path*> v);

        /********************************************/
        /** GETS & SETS                             */
        /********************************************/

        int getId();

        Node* getNext();

        void setNext(Node* next);

        vector<Path*> getAdjacencyList();


    private:
        //ID
        int id;

        //Pointer to next node
        Node* next;

        //Adjacent
        vector<Path*> adjacency;
};

#endif // NODE_H
