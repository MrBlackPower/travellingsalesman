#include "path.h"

Path::Path(Node* n1, Node* n2, int weight)
{
    this->n1 = n1;
    this->n2 = n2;
    this->weight = weight;
}

Path::~Path(){

}

Node* Path::getStart(){
    return n1;
}

Node* Path::getEnd(){
    return n2;
}

int Path::getWeight(){
    return weight;
}
